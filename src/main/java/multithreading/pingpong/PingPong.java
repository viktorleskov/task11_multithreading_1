package multithreading.pingpong;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PingPong {
    private static Logger LOG = LogManager.getLogger(PingPong.class);
    private static Object monitor = new Object();
    Thread t1;
    Thread t2;

    public void ping() {
        t1 = new Thread(() -> {
            synchronized (monitor) {
                while (true) {
                    try {
                        monitor.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    LOG.info("ping");
                    monitor.notify();
                }
            }
        });
    }

    public void pong() {
        t2 = new Thread(() -> {
            synchronized (monitor) {
                while (true) {
                    monitor.notify();
                    try {
                        monitor.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    LOG.info("pong");
                }
            }
        });
    }
    public void startPingPong(){
        ping();pong();
        t1.start();
        t2.start();
    }
}
